#!/usr/bin/env python
import bs4
import urllib2
import requests
import praw
import pdb
import re
import os
from bs4 import BeautifulSoup
from lxml import html

# reddit = praw.Reddit('bot1')
# subreddit = reddit.subreddit('pythonforengineers') #fetch subreddit

# Get site and soup it
gameSite='http://www.espn.com/nba/boxscore?gameId=401070213'
f=open('/users/gavininglis/documents/webdev/sixersbot/espnData.txt','w')
error=open('/users/gavininglis/documents/webdev/sixersbot/errorEspn.txt','w')
soup=BeautifulSoup(urllib2.urlopen(gameSite),"html.parser")

####### GENERAL OVERVIEW ######

# Get Team Names
teamNames=[teamName.getText() for teamName in soup.findAll('span',{'class': 'team-name'})]
team1=teamNames[0]
team2=teamNames[1]

# Scores and Time
score1=soup.find('div',{'class' : 'score icon-font-after'}).getText()
score2=soup.find('div',{'class' : 'score icon-font-before'}).getText()
time=soup.find('span',{'class' : 'status-detail'}).getText() #game-time status-detail

#############################################################################

##### STATS AND BOXSCORE #####
# <div id="gamepackage-box-score" data-module="boxscore"  data-sport="basketball">
# ^^^ this is all the box score info, children are player names and stats
# post = subreddit.submit(subreddit,team1+ ' vs. ' +team2,'')
playerNames=[playerName.getText() for playerName in soup.findAll('span',{'class': 'abbr'})]
team1players=playerNames[0:13]
team2players=playerNames[13:26]


# Find FGs and FG percentages for teams
#t=[fgs.getText() for ts in soup.findAll('tr',{'class':'highlight'})]

################ TEAM TOTALS ################
tableRows = soup.findAll('tr',{'class':'highlight'})
teamTotals=[]
# loop throw tr with class highlight, append data to array
# 0 fg totals 1 3pt totals 2 ft totals 3 ored 4 dreb 5 assists 6 steals 7 blocks 8 turnovers
# 9 fg% 10 3pt% 11 ft%
# team 1 0:12, team 2 12:24

for tr in tableRows:
    tdfg = tr.findAll('td',{'class':'fg'})
    for fg in tdfg:
        teamTotals.append(fg.text)
    td3 = tr.findAll('td',{'class':'3pt'})
    for i in td3:
        teamTotals.append(i.text)
    tdft = tr.findAll('td',{'class':'ft'})
    for ft in tdft:
        teamTotals.append(ft.text)
    tdoreb = tr.findAll('td',{'class':'oreb'})
    for oreb in tdoreb:
        teamTotals.append(oreb.text)
    tddreb = tr.findAll('td',{'class':'dreb'})
    for dreb in tddreb:
        teamTotals.append(dreb.text)
    tdast = tr.findAll('td',{'class':'ast'})
    for ast in tdast:
        teamTotals.append(ast.text)
    tdstl = tr.findAll('td',{'class':'stl'})
    for stl in tdstl:
        teamTotals.append(stl.text)
    tdblk = tr.findAll('td',{'class':'blk'})
    for blk in tdblk:
        teamTotals.append(blk.text)
    tdto = tr.findAll('td',{'class':'to'})
    for to in tdto:
        teamTotals.append(to.text)

# remove blank entries to teamTotals
teamTotals = filter(None, teamTotals)

######## PLAYER STATS  ############
playerStats=[]
# same format as total stats
# 0 fg totals 1 3pt totals 2 ft totals 3 ored 4 dreb 5 assists 6 steals 7 blocks 8 turnovers
# 9 fg% 10 3pt% 11 ft%
# team 1 0:12, team 2 12:24
p_tableRows = soup.findAll('tr') # arguments here might be too generalized, could be more specific
for tr in p_tableRows:
    tdfg = tr.findAll('td',{'class':'fg'})
    for fg in tdfg:
        playerStats.append(fg.text)
    td3 = tr.findAll('td',{'class':'3pt'})
    for i in td3:
        playerStats.append(i.text)
    tdft = tr.findAll('td',{'class':'ft'})
    for ft in tdft:
        playerStats.append(ft.text)
    tdoreb = tr.findAll('td',{'class':'oreb'})
    for oreb in tdoreb:
        playerStats.append(oreb.text)
    tddreb = tr.findAll('td',{'class':'dreb'})
    for dreb in tddreb:
        playerStats.append(dreb.text)
    tdast = tr.findAll('td',{'class':'ast'})
    for ast in tdast:
        playerStats.append(ast.text)
    tdstl = tr.findAll('td',{'class':'stl'})
    for stl in tdstl:
        playerStats.append(stl.text)
    tdblk = tr.findAll('td',{'class':'blk'})
    for blk in tdblk:
        playerStats.append(blk.text)
    tdto = tr.findAll('td',{'class':'to'})
    for to in tdto:
        playerStats.append(to.text)

playerStats = filter(None, teamTotals)

fg=[fgs.getText() for fgs in soup.findAll('td',{'class':'fg'}) if fgs not in soup.findAll('tr',{'class':'highlight'})]
for char in fg:
    if re.search(r'%',char,re.M|re.I):
        fg.remove(char)

team3s=[threes.getText() for threes in soup.findAll('td',{'class':'3pt'})]
for char in team3s:
    if re.search(r'%',char,re.M|re.I):
        team3s.remove(char)

####### ENCODING LISTS ######
# beautifulSoup always encodes in unicode, convert to utf-8 here #
teamTotals=[x.encode('utf-8') for x in teamTotals]
team1players=[x.encode('utf-8') for x in team1players]
team2players=[x.encode('utf-8') for x in team2players]
team3s=[x.encode('utf-8') for x in team3s]
fg=[x.encode('utf-8') for x in fg]


selfText=team1 + ' vs. ' + team2 + '\n'+ time + '\n' +' Player | FG '+'\n'


reddit = praw.Reddit('bot1')
subreddit = reddit.subreddit('pythonforengineers') #fetch subreddit
post = subreddit.submit(team1+ ' vs. ' +team2, selfText)
post_id=post.id

##### UPDATING OF STATS TO POST







#### PRINT CHECK ####
print(teamTotals[0:12]) # team 1 totals
print(teamTotals[12:24]) # team 2 totals


print(team1+ ' ' +team2)
print('   ' + str(score1) + '   '+ str(score2))
print(time)

print(team1+ ' Players')
print(team1players)
print(team2 + ' Players')
print(team2players)

print(fg)
print(team3s)

# write to file
score1=int(score1)
score2=int(score2)
f.write(str(team1)+ ' '+str(score1)+'\n')
f.write(str(team2)+' '+str(score2) +'\n')
f.close
